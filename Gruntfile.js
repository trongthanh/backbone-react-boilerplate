/* © 2014
 * @author Thanh Tran
 */
/*jshint node:true*/
module.exports = function(grunt) {
	'use strict';
	// show elapsed time at the end
	require('time-grunt')(grunt);
	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	/*  ========================================================================
								CUSTOM TASKS
	======================================================================== */


	// Project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		/**
		 * DEV: script combined but not minified (except libraries, css), all mocks intact, no external services
		 * SIT: script combined but not minified (except libraries, css), use testing services
		 * UAT: script combined and minified, use UAT services
		 * PRO: script combined and minified, use PROD services
		 *
		 * @type {String}
		 */
		mode: 'DEV',

		dir: {
			app: 'src',
			dist: '~build-<%= mode %>',
			assets: 'assets'
		},

		// Task configuration

		// Reads HTML for usemin blocks to enable smart builds that automatically
		// concat, minify and revision files. Creates configurations in memory so
		// additional tasks can operate on them
		useminPrepare: {
			options: {
				root: '<%= dir.app %>/',
				dest: '<%= dir.dist %>/'
			},
			html: '<%= dir.app %>/index.html'
		},

		// Performs rewrites based on rev and the useminPrepare configuration
		// The :js target is deliberately commented out, need careful review before using filerev task
		// Currently, there are dynamic image path generation in JS, that stop us to transform image file names for now
		usemin: {
			options: {
				assetsDirs: ['<%= dir.dist %>', '<%= dir.dist %>/img', '<%= dir.dist %>/css']
				// patterns: {
				// 	js: [
				// 		[/(img\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images']
				// 	]
				// }
			},
			html: ['<%= dir.dist %>/{,*/}*.html'],
			css: ['<%= dir.dist %>/css/{,*/}*.css', '!<%= dir.dist %>/css/main.css']
			// js: '<%= dir.dist %>/js/{,*/}*.js'
		},

		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= dir.app %>/',
					dest: '.tmp/',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'{,*/}*.html',
						'js/**/*.{js,json,jsx}',
						'!js/lib/**/*.js', // exclude the libraries, use next task
						'data/**/*.{js,json,jsonp}',
						'mock/**/*.{json,jpg,png,svg}',
						'img/{,*/}*.svg',
						'fonts/{,*/}*.*',
						'plugins/**/*'
						//some js files need to be specified explicitly

					]
				}]
			},
			// Copy the libraries with the .min.js files renamed as main .js files
			// Reason is minifying these libraries takes a lot of time
			// So we are keeping these minified files and copy them over during building
			lib: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= dir.app %>/js/lib',
					dest: '.tmp/js/lib',
					src: [
						'*.js',
						'!*.min.js'
					]
				}, {
					'.tmp/js/lib/backbone.js': '<%= dir.app %>/js/lib/backbone.min.js',
					'.tmp/js/lib/c3.js': '<%= dir.app %>/js/lib/c3.min.js',
					'.tmp/js/lib/react.js': '<%= dir.app %>/js/lib/react.min.js'
				}]
			}
		},

		sass: {
			dev: { // Target
				options: { // Target options
					outputStyle: 'expanded'
					// sourceMap: true // temporary disable
				},

				files: [{
					expand: true,
					cwd: '<%= dir.app %>/css/',
					src: ['*.scss'],
					dest: '<%= dir.app %>/css/',
					ext: '.css'
				}]
			},
			dist: { // Target
				options: { // Target options
					outputStyle: 'compressed'
				},

				files: [{
					expand: true,
					cwd: '<%= dir.app %>/css/',
					src: ['*.scss'],
					dest: '<%= dir.dist %>/css/',
					ext: '.css'
				}]
			}
		},

		autoprefixer: {
			options: {
				browsers: ['last 2 version', 'ie >= 9', 'opera >= 12']
			},
			dist: {
				src: '<%= dir.dist %>/css/*.css'
			},
			dev: {
				options: {
					// map: true //enable sourcemaps
				},
				src: '<%= dir.app %>/css/*.css'
			}
		},

		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= dir.app %>/img',
					src: '{,*/}*.{gif,jpeg,jpg,png}',
					dest: '<%= dir.dist %>/img'
				}]
			}
		},

		filerev: {
			dist: {
				src: [
					'<%= dir.dist %>/img/**/*.{jpg,jpeg,gif,png,webp,svg}',
					'!<%= dir.dist %>/img/social/*.{jpg,jpeg,png}',
					'<%= dir.dist %>/css/**/*.{css,eot,ttf,woff}',
					'<%= dir.dist %>/js/*.js'
				]
			}
		},

		webfont: {
			icons: {
				src: '<%= dir.assets %>/icons/*.svg',
				dest: '<%= dir.app %>/fonts/',
				destCss: '<%= dir.app %>/css/common',
				options: {
					template: '<%= dir.assets %>/webfont-templates/bootstrap.css',
					hashes: false, //supress hash generation
					types: 'eot,ttf,woff',
					stylesheet: 'scss',
					syntax: 'bootstrap',
					relativeFontPath: '../fonts/', //fix the relative font path due to different location of concatenated CSS
					destHtml: '<%= dir.app %>' //for icon font testing
				}
			}
		},

		// Install grunt-favicons package in order to run this task
		// This task should only be run once when changing the favicon and webapp icons
		// The favicon.ico should still be generated by an online service
		// favicons: {
		// 	options: {
		// 		appleTouchBackgroundColor: '#ffffff',
		// 		html: '<%= dir.app %>/index.html'
		// 	},
		// 	icons: {
		// 		src: '<%= dir.assets %>/favicon/source.png',
		// 		dest: '<%= dir.app %>/'
		// 	}
		// },

		preprocess: {
			dist: {
				src: [
					'.tmp/js/**/*.js', '!.tmp/js/lib/*.js',
					'.tmp/*.html'
				],
				options: {
					inline: true,
					context: {
						APP_ID: '<%= pkg.name %>'
						// the equivalent DEV|SIT|UAT|PRO flag will be set through build task
					}
				}
			}
		},

		/**
		 * r.js will concat JS and copy whole project structure from .tmp to final dir.dist
		 */
		requirejs: {
			dist: {
				options: {
					//r.js configurations:
					appDir: '.tmp/',
					baseUrl: 'js/lib',
					mainConfigFile: '.tmp/js/main.js',
					dir: '<%= dir.dist %>',
					paths: {
						// in main config, the lang part
						'data/translations': '../../data/translations-es.json'
						// additional path fixing
					},
					// fileExclusionRegExp: /\.jsx/,
					uglify2: {
						except: ['jquery.js', 'backbone.js']
					},
					optimize: 'none',
					removeCombined: true,

					stubModules: ['jsx', 'JSXTransformer'],

					modules: [{
						name: 'jquery',
						include: [
							// include jquery plugins here
						],
						exclude: []
					}, {
						name: '../main',
						include: ['app/AppView'],
						exclude: [
							'jquery',
							'text!data/translations',
							'react' // these modules for uncompiled jsx
						]
					}]
				}
			}
		},

		// Note that this task is going through grunt-jsxhint which can validate both .js and .jsx files
		jshint: {
			options: {
				jshintrc: true
			},
			app: {
				src: ['<%= dir.app %>/js/**/*.{js,jsx}', '!<%= dir.app %>/js/lib/*.js']
			}
		},

		watch: {
			sass: {
				files: [
					'<%= dir.app %>/css/**/*.scss'
				],
				tasks: ['css-dev']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'<%= dir.app %>/*.html',
					'<%= dir.app %>/js/**/*.{js,jsx}',
					'<%= dir.app %>/css/*.css',
					'<%= dir.app %>/data/*.json'
				]
			}
		},
		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= dir.dist %>'
					]
				}]
			}
		},

		connect: {
			options: {
				port: 8000,
				livereload: 35729,
				hostname: '0.0.0.0',
				// Handle POST requests
				middleware: function(connect, options, middlewares) {
					var fs = require('fs');
					var path = require('path');
					var support = ['POST', 'PUT', 'DELETE'];
					// inject a custom middleware into the array of default middlewares
					middlewares.unshift(function(req, res, next) {
						// enable POST and any non-GET requests
						if (support.indexOf(req.method.toUpperCase()) !== -1) {
							grunt.log.writeln(req.method + ' request:', req.url);
							res.setHeader('Access-Control-Allow-Origin', '*');
							res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

							var filepath = path.join(options.base[0], req.url);
							if (fs.existsSync(filepath) && fs.statSync(filepath).isFile()) {
								return res.end(fs.readFileSync(filepath));
							}
						}

						return next();
					});

					return middlewares;
				}
			},
			livereload: {
				options: {
					open: true, //open nearest browser automatically
					base: [
						'<%= dir.app %>'
					]
				}
			},
			build: {
				options: {
					keepalive: true, // prevent the task to stop
					open: true, //open nearest browser automatically
					base: [
						'<%= dir.dist %>'
					]
				}
			}
		},

		notify: {
			build: {
				options: {
					title: 'Build Complete',  // optional
					message: 'Build process <%= mode %> completed.' //required
				}
			}
		}
	});

	//Tasks compositions
	grunt.registerTask('css-dev', 'Compile SCSS for developement', ['sass:dev', 'autoprefixer:dev']);

	grunt.registerTask('test', 'Validate and test the code', function() {
		grunt.task.run([
			'jshint'
		]);
	});

	/**
	 * Build task,
	 * @param  {[type]} mode [description]
	 * @return {[type]}      [description]
	 */
	grunt.registerTask('build', 'The main build task, accept parameters to switch mode',
		function(mode) {
			// default mode if not defined in param
			if (!mode || ['DEV', 'SIT', 'UAT', 'PRO'].indexOf(mode.toUpperCase()) === -1) {
				grunt.log.warn('Mode :' + mode + ' not defined or is unknown. Default to DEV');
				mode = 'DEV';
			} else {
				mode = mode.toUpperCase();
			}

			// apply config
			grunt.config.set('mode', mode);
			// set process flag
			grunt.config.set('preprocess.dist.options.context.' + mode, true);

			// fine tune the tasks
			if (mode === 'SIT' || mode === 'UAT' || mode === 'PRO') {
				// something unique to services integrated
			}

			if (mode === 'UAT' || mode === 'PRO') {
				// something unique to UAT & PRO
			}

			if (mode === 'PRO') {
				// something unique to PROduction
			}

			grunt.task.run([
				// 'test',
				'clean:dist',

				// JS compilation
				'copy:dist',
				'copy:lib',
				'preprocess:dist',
				'requirejs:dist',

				// SASS compilation
				'sass:dist',
				'autoprefixer:dist',

				// static files & plugins compilation
				'useminPrepare',
				'imagemin',
				'concat:generated',
				'cssmin:generated',
				// 'filerev', // deliberately comment out, see comments @ usemin
				'usemin',
				'notify:build'
			]);
		});

	grunt.registerTask('serve', 'Serve the app on browser for testing', function() {
		grunt.task.run([
			'css-dev',
			'connect:livereload',
			'watch'
		]);
	});


	grunt.registerTask('serve-build', 'Serve the build folder on browser for testing, this task doesn\'t include watch tasks',
		function(mode) {
			mode = mode ? mode.toUpperCase() : 'DEV';

			// apply mode config
			grunt.config.set('mode', mode);

			grunt.task.run([
				'connect:build'
			]);
		});

	// Default task
	grunt.registerTask('default', ['serve']);
};
