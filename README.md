React Backbone Boilerplate
========================

- GIT path: https://bitbucket.org/trongthanh/backbone-react-boilerplate

GETTING STARTED WITH DEVELOPMENT
--------------------------------

1. Software installation:
	- Code editor of your choice (Sublime Text is recommended and project files are included)
	- Google Chrome 36+
	- Ruby runtime (default in OSX) & [SASS][] (via gem)
	- [NodeJS][] (for testing, previewing, compiling and optimizing processes)
	- [GruntJS][] commandline tool: `npm install -g grunt-cli`
2. Setting up IDE/Editor
	- Install development dependencies: Open termnial, change dir to **this folder** (where `package.json` file is located) and execute `npm install`
	- If using Sublime Text, just open the `*.sublime-project` and start coding
	- Else, create project with IDE of your choice, with project folder pointing to **this folder** (where this file is located)
3. Previewing app in web browser (any compilation will be done and updates will be refreshed automatically):
	- Execute `grunt serve`
4. Build the app for server deployment:
	- Execute `build:DEV`
	- Change the flag `:DEV` to either `:SIT`, `:UAT`, `:PRO`
	- Read `Gruntfile.js` > `mode` config for the flag details
	- Execute `grunt serve-build:MODE` to serve the build folder to browser for quick previewing

VERSIONING
----------
- Version numbers will be as follows: [RELEASE].[SPRINT].[JENKIN_BUILD_NUMBER]

TECHNICAL SOLUTIONS
-------------------
- MVC Approach: [Backbone][] is used to provide the base for Model( and Collection) and View classes
- [Classical Inheritance][]: Object's functionalities reuseable and inheritable based on Prototype chain & constructor functions
- [RequireJS][] and [AMD][]: Library to load .js as module and resolve dependency. Helping separate classes and modules into different .js files, so that code maintenance is easier. Later, optimization script will be used to concatenate all the modules and minify them.
- [jQuery][]: Library to help manipulate DOM, CSS.
- [ReactJS][]: High performance HTML templating & DOM simulation library. We are using [Require JSX Plugin][] to load ReactJS component on the fly. Note: We use react-with-addons.js replacing for react.js to use builtin addons of react.

DEVELOPMENT DEPENDENCIES
------------------------
- Ruby
- SASS
- NodeJS
- Grunt (global `grunt-cli`)
- Refer to devDependencies in `package.json`

BONUS INSTALLATIONS
-------------------
We are using Sublime Text 3 as our primary code editors. Install these plugins
will enhance productivity and allow more enjoyable development experiences

(Links are TBD)
- Sublime Text plugins:
	+ SublimeLinter
	+ SublimeLinter-jshint
	+ SublimeLinter-jsxhint
	+ SublimeLinter-jscs
	+ EditorConfig 
	+ ReactJS
	+ Sass
	+ Emmet
- NodeJS packages (global):
	+ jshint (for use with SublimeLinter-jshint)
	+ jsxhint (for use with SublimeLinter-jsxhint)
	+ jscs (for use with SublimeLinter-jscs)

UNIT TESTING
------------
[TBD]

FOLDER STRUCTURE
----------------
	/
	├── assets                  : (directory for assets like PSDs, install templates)
	├── ~build-MODE             : build outputs, ignored from version control
	├── src                     : main application source code
		├── css                 : SASS/CSS files
		├── fonts               : contains web font / icon font files
		├── img                 : images
		├── mock                : mock data and services (will be excluded from SIT & UAT & PRO build mode)
		├── data                : static JSON or other data files for configuration (translations, customizable lists...)
		├── plugins             : other plugins (CSS & JS)
		└── js                  : JavaScript files
			├── lib             : 3rd-party libraries (jQuery, requireJS, Backbone...)
			└── app             : app namespace
				|                   all application source placed in here
				├── collections : Backbone collections
				├── enums       : Enumerable modules
				├── events      : Event modules
				├── models      : Backbone models
				├── views       : Backbone + React views
				└── helpers     : Helper classes (normally Static classes)
	├── test                    : contains files related to Javascript unit testing
		├── lib                 : contains javascript files required for unit testing
		├── results             : contains output files from running the unit tests
		└── spec                : place all Jasmine unit tests in this folder
	└── tools                   : bundled scripts and tools

CONVENTIONS & BEST PRACTICES
----------------------------
- Refer to `.jscsrc` for detailed coding styles
- Alignment by TABs (not SPACES, tab width is up to user's preference, but 4-space tab is recommended)
- Single quotes ('...') for String literal in js files
- Variable Naming:
	+ Prefix property name with `_` (underscore) if it is intended as private and should not be used outside of the class
	+ Prototypes, classes (base objects that are used to spawn instances) are named with __PascalCase__ (Pin, PinBoard)
	+ React component classes (factory function used in `React.renderComponent()`) are named with __PascalCase__ (LoginForm, Sidemenu) to follow examples in ReactJS documentation
	+ Global objects, singleton and static objects are named with __PascalCase__ (DataModel, AppView, Templates)
	+ Object instances are named with __camelCase__ (pin, pinBoard)
	+ Prefix jQuery object variables with `$` sign so it is easier to differentiate jQuery objects with HTML element reference and other variable types. Example: `$view, $el, $scene`
	+ Declare variables at top of functions, group variables which are not initializeed with values into a one-liner `var` statement
	```JavaScript
		var a, b, c;
		var name = 'Default';
		var age = 20;
	```
- Functions:
	+ Prefix ReactJS event handler with 'handle' to follow ReactJS documentation
	+ Prefix function name with 'on' if it is an ordinary event handling function
	+ Prefix function name with '_' if it is intended as private and should not be used outside of the class
- Local variable for `this` reference: name it `self`
- Avoid using global $(selector) in View components, use local $el.find(selector) OR the shortcut this.$(selector) instead
- OOCSS:
	+ NO IDs in CSS
	+ Avoid attaching classes to elements (i.e. don’t do div.header or h1.title)
	+ Except in some rare cases, avoid using !important
	+ Separate structure and skin: define repeating visual features (like background and border styles) as separate “skins” that you can mix-and-match
	+ Separate container and content: rarely use location-dependent styles, an object should look the same no matter where you put it.
- jshint & jsxhint MUST BE USED to validate JavaScript syntax to maintain sanity & clarity of the code.
	Refer to `.jshintrc` for detailed global rules
- Comment and documentation:
	+ Comment every module/class
	+ Comment every public function
	+ Comment every CSS component/file (at high level)
	+ Explicitly mark a function with `@override` if it is an override function
	+ Make use of [JSDoc][] syntax for auto-documentation generation later
	+ See DOCUMENTATION BOILERPLATES below
- SASS: prefix underscore `_` to .scss files that are going to be included, not compiled separately.
- Autoprefixer: Don't add browser prefixes by yourself, let the grunt task autoprefixer do it for you.
- RequireJS Module template:
```JavaScript
/* © 2014 copyright holder
 * @author Author Full Name
 */
define(function(require) {
	// imports
	var React = require('react');
	var Module = require('path/to/module');
	var classSet = React.addons.classSet;
	
	// class / module definition
	var MyClass = function () {
		//Class constructor
	};

	/* ... */

	// exports
	return MyClass;
});
```
- REACTJS best pratices:
	+ Import ReactJS's add-ons at the top of module definition, together with other imports statements, for clarity.
	+ Try using ReactJS's state management instead of traditional jQuery's DOM manipulation. By using React states, components is easier to maintained and its states will be consistent from logic to UI.
	+ React component's `props` are not mutable, they should be updated from parents and outside; while `state` used to keep its different states, should only be updated by the component itself.
	+ Do not call another method that try nested-rendering or React.renderComponent() inside `render` method. `render` function should contain pure props and states accessing and DOM creation.
	+ Always add `key` prop to dynamically generated items of a React DOM array (e.g array of LI). This is for rendering efficiency.

PREREQUISITES
-------------
Besides basic HTML5, CSS3, JS knowledge, these prerequisites must be met:

### HTML5

1. Semantic HTML, data-* attributes
2. New input types (button, email, tel, date...)
3. Built-in input validation

### CSS3 + SASS

1. Responsive web design with relative measure values: `em, %`
2. box-sizing: border-box
3. SASS: variables, mixins, functions, some built-in functions, nested selectors

### JavaScript

2. Function scope & `this` reference
3. `function.apply(), function.call(), function.bind()`
4. [Classical Inheritance][] in JS (prototypal inheritance with constructor pattern)
6. Basic knowledge of module pattern, AMD, Require JS
7. [ReactJS][]
8. Basic vanilla HTMLElement API (getAttribute, dataset, classList, style...)
9. Basic JavaScript optimization tips

### Tooling

1. A code editor 
	- With JSHint, JSCS validation integrated
	- With decent HTML, CSS, JavaScript, SASS code hinting
	- Snippets & basic auto completion
2. SASS command line, NodeJS command line
3. Webkit browser for testing / preview, preferably Google Chrome 28++
4. Intermediate browser's devtools / debugger know-how
5. Photoshop for exporting artwork, collecting graphics info
6. Vector editing software (preferrably Illustrator)


KNOWLEDGE BASE:
---------------
1. We are not using HTML template engine other than one built in ReactJS, therefore, as rules of thumb:
	- For any components which require rendered markup, let's create ReactJS component (.jsx)
	- For views that already has markup in index.html, create a Backbone View wrapper.
2. Here are list of known HTML5 features that may require polyfills
	- All polyfills required by ReactJS: [http://facebook.github.io/react/docs/working-with-the-browser.html#polyfills-needed-to-support-older-browsers]
	- Below are polyfills required by our own app:
		- element.classList
		- Function.prototype.bind()
		- Array.prototype.map()
		- Object.assign(): polyfill included


[AMD]: http://requirejs.org/docs/whyamd.html
[Backbone]: http://backbonejs.org/
[Classical Inheritance]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Inheritance_and_the_prototype_chain#With_a_constructor
[H5BP]: http://html5boilerplate.com
[ReactJS]: http://facebook.github.io/react/
[jQuery]: http://jQuery.com
[NodeJS]: http://nodejs.org/
[Phonegap]: http://phonegap.com
[RequireJS]: http://requirejs.org
[SASS]: http://sass-lang.com/
[JSDoc]: http://usejsdoc.org/
[JasmineJS]: http://jasmine.github.io/2.0/introduction.html
[Require JSX Plugin]: https://github.com/philix/jsx-requirejs-plugin