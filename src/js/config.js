/* © 2014 Aleph-labs.com
 * @author Thanh Tran, Quy Tran
 */
/**
 * Application config, as static object
 * Will be changed by preprocessor
 */
define({
	SERVICE_URL_ROOT: 'mock/',

	SERVICE_LOGIN: 'login.json',
	SERVICE_CLIENT_LIST: 'client-list.json',
	SERVICE_BRIEF_CLIENTS_LIST: 'brief-clients-list.json',
	SERVICE_CLIENT_STREAM: 'client-stream.json',
	SERVICE_REPORT_LIST: 'report-list.json',
	SERVICE_CLIENT_DETAILS: 'clients/${id}.json',

	/*Format*/
	DATE_FORMAT: 'YYYY-MM-DD',
	DISPLAY_DATE_FORMAT: 'DD MMM YYYY',
	NEWS_URL: 'http://news.google.com.co/news?pz=1&cf=all&ned=es_co&hl=es&topic=b&output=rss',
	YAHOO_WEATHER_URL: 'https://query.yahooapis.com/v1/public/yql?format=json'
});
