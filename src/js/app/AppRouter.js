/* © 2014 Aleph-labs
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var Backbone = require('backbone');
	var AppModel = require('app/AppModel');

	/**
	 * The Singleton Application Router
	 *
	 * @class AppRouter
	 * @namespace app
	 */
	var AppRouter = Backbone.Router.extend({
		/**
		 * AppView instance
		 * @type {[type]}
		 */
		appView: null,

		routes: {
			'welcome': 'welcome', // page welcome and login
			'logout': 'logout',
			'dashboard': 'dashboard',
			'dashboard/': 'dashboard',
			'dashboard/portfolio': 'portfolio',
			'dashboard/portfolio/': 'portfolio',
			'dashboard/:level1': 'dashboard',
			'dashboard/:level1/': 'dashboard',
			'dashboard/:level1/:level2': 'dashboard',
			'dashboard/:level1/:level2/*deepTarget': 'dashboard',
			'*actions': 'defaultAction' // math #anything-here, used for all logged-in page for now
		},

		loggedinRoutes: [
			'dashboard'
		],

		initialize: function() {
			AppModel.session().on('change:sessionId', function() {
				if (this._checkLogin()) {
					console.log('AppRouter::session sync navigate to /dashboard');
					this.navigate('dashboard', {trigger: true});
				} else {
					// logged out
					this.navigate('welcome', {trigger: true});
				}
			}, this);
		},
		/**
		 * Start Backbone.history
		 * Only start this when AppView is started
		 * @return {[type]} [description]
		 */
		start: function(appView) {
			if (!appView) { throw new Error('AppRouter::start(): appView param is not defined'); }
			this.appView = appView;
			// Start Backbone history a necessary step for bookmarkable URL's
			Backbone.history.start();
		},
		defaultAction: function(action) {
			console.log('AppRouter::default handle', action);
			if (this._checkLogin()) {
				if (action && this.loggedinRoutes.indexOf(action.toLowerCase()) !== -1) {
					this.appView.render(action);
				} else {
					this.navigate('dashboard/news', {replace: true, trigger: true});
				}
			}
		},

		/**
		 * Welcome page route
		 * @return {[type]} [description]
		 */
		welcome: function() {
			console.log('AppRouter::welcome route handler');
			AppModel.session().logout();
			this.appView.render('welcome');
		},

		/**
		 * Logout route
		 * @return {[type]} [description]
		 */
		logout: function() {
			console.log('AppRouter::welcome route handler');
			AppModel.session().logout();
		},

		_checkLogin: function() {
			var isLoggedIn = AppModel.session().isLoggedIn();
			console.debug('AppRouter::_checkLogin:isLoggedIn', isLoggedIn);
			if (!isLoggedIn) {
				this.navigate('/welcome', {trigger: true, replace: true});
			}
			return isLoggedIn;
		},

		/**
		 * bare 'dashboard' route, always redirect to 'news' subpage
		 * @return {[type]} [description]
		 */
		dashboard: function(level1, level2, deepTarget) {
			if (this._checkLogin()) {
				if (!level1) {
					// just #dashboard
					this.navigate('dashboard/news', {replace: true, trigger: true});
				} else {
					this.appView.render('dashboard', {level1: level1, level2: level2, deepTarget: deepTarget});
				}
			}
		}

	});

	// exports
	return new AppRouter();
});