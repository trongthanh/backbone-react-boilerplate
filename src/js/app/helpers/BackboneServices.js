/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var Backbone = require('backbone');
	var $ = require('jquery');

	/**
	 * Backbone ajax wrapper and other enhancements
	 * Please load this helper once in main or AppModel
	 *
	 */

	/**
	 * Ajax wrapper, augment the service responses to seamlessly set data for the model.
	 * Strip any extra status data from the json object.
	 *
	 * Typical response object will have this status header:
	 * ```
	 *  "header": {
	 *     "appName": "bancolombia",
	 *     "user": "sparm2",
	 *     "responseDateTime": "2014-07-11T12:39:51Z",
	 *     "language": "en",
	 *     "country": "sg",
	 *     "serviceName": "login",
	 *     "status": "1", // 1 mean OK
	 *     "appVersion": "1.0"
	 * },
	 * ```
	 * We'll check for status code and descide whether this is success or not.
	 * Later, we'll also use it to check for login session.
	 *
	 * @return {[type]} [description]
	 */
	Backbone.ajax = function(options) {
		var success = options.success;

		options.success = function(data) {
			if (options.externalAPI) {
				success(data);
				return;
			}
			if (data.header) {
				// no issues
				/*jshint eqeqeq:false*/
				if (data.header.status == '1') {
					// move the status header to xhr object in options
					if (options.xhr) { options.xhr.serviceHeader = data.header; }
					delete data.header;

					// call the original success handler with new data sans 'header' property
					success(data);
				} else if (options.error) {
					// call the error handler
					options.error(options.xhr);
				}
			} else {
				throw new Error('Ajax response not standard. Missing status header.');
			}
		};

		/*@ifdef DEV*/
		var errorHandler = options.error;
		options.error = function(error) {
			if (error.status === 404 && error.responseText.indexOf('mock/clients') !== -1) {
				console.warn('Mock JSON not found. Please execute `grunt prepare-mock-data` to generate mock data.');
			}
			if (typeof errorHandler === 'function') {
				errorHandler(error);
			}
		};
		/*@endif*/

		// Execute the jquery ajax
		$.ajax.apply($, arguments);
	};

	// exports
	return Backbone;
});