/* © 2013 aleph-labs.com
 * @author Thanh Tran
 */
/**
 * Manage LocalStorage with seamless object conversions
 * Currently supports conversion of String, Number, Date, Object, Array objects
 * Note: Date object if go through JSON.stringify() will become date string in ISO format
 * @class  LocalStorage
 * @namespace app.helpers
 * @version 20141006
 */
define(function() {
	'use strict';
	//imports
	var localStorage = window.localStorage;

	// properties
	var dateReg = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/;

	/**
	 * Return stored object with assigned name
	 * @method getItem
	 * @static
	 * @param  {String} name       name of the object
	 * @param  {String} type       used to assist the type conversion of item, if not define, always try to parse as object
	 * @return {*}                 object value
	 */
	function getItem(name, type) {
		var strVal = localStorage.getItem(name),
			obj;

		type = String(type).toLowerCase();

		if (strVal === null) {
			return null;
		}

		if (type === 'date') {
			return new Date(strVal);
		} else if (type === 'number') {
			return parseFloat(strVal);
		} else if (type === 'string') {
			return strVal;
		} else {
			try {
				obj = JSON.parse(strVal);
				convertTypes(obj);
				return obj;
			} catch (err) {
				//item is likely a string
				return strVal;
			}
		}
	}

	/**
	 * simple recursive type conversion
	 * @param  {Object} obj
	 * @private
	 */
	function convertTypes(obj) {
		var val;
		for (var prop in obj) {
			val = obj[prop];
			if (dateReg.test(val) && !isNaN(Date.parse(val))) {
				obj[prop] = new Date(val);
			} else if (typeof val === 'object') {
				convertTypes(val);
			}
		}
		return obj;
	}

	/**
	 * save item to local storage
	 * @param {String} name name of item
	 * @param {Object} data data to save, must be Object
	 */
	function setItem(name, data, type) {
		var t = String(type).toLowerCase();
		if (t === 'string' ||
			t === 'date' ||
			t === 'number') {
			localStorage.setItem(name, data);
			return true;
		} else {
			try {
				localStorage.setItem(name, JSON.stringify(data));
				return true;
			} catch (err) {
				return false;
			}
		}

	}

	function removeItem(name) {
		localStorage.removeItem(name);
	}

	function removeAll() {
		localStorage.clear();
	}

	//exports
	return {
		getItem: getItem,
		setItem: setItem,
		removeItem: removeItem,
		removeAll: removeAll
	};
});
