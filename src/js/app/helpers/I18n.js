/* © 2014 aleph-labs.com
 * @author Thanh Tran
 */
define(function (require) {
	'use strict';
	//imports
	var moment = require('moment');
	require('moment.locale.es'); // just load in the locale plugin for Moment
	var React = require('react.backbone');
	var translations = JSON.parse(require('text!data/translations'));

	/**
	 * Preliminary internationalization helper class

	 * TODO: Need to define standard API and implement for translation, interpolation and pluralization
	 * I'm taking this lib: https://github.com/fnando/i18n-js as API reference and we may/may not use that library instead of
	 * comming up a lightweight custom approach
	 * Right now we only support direct property access
	 *
	 * For date formatting, we'll use moment.js as described below
	 *
	 * @class I18n
	 * @namespace app.helper
	 */
	var I18n = {

		/**
		 * Get the translated text from the text id
		 *
		 * @param  {string} id Translation text id, can be nested object property notation
		 * @return {string}    Translated string
		 * @method t
		 */
		t: function (id) {
			var str = this._propByString(translations, id);
			if (str) {
				return str;
			} else {
				console.log(console.WARNING, 'I18n::t:Translation item', id, 'is undefined');
				return id;
			}
		},

		/**
		 * A neat solution by this Stackoverflow:
		 * http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
		 * @param  {Object} o Object to get the prop from
		 * @param  {String} s Property access notation, e.g: `page.header.title`
		 * @return {String}   [description]
		 */
		_propByString: function(o, s) {
			s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
			s = s.replace(/^\./, '');           // strip a leading dot
			var a = s.split('.');
			while (a.length) {
				var n = a.shift();
				if (n in o) {
					o = o[n];
				} else {
					return '';
				}
			}
			return o;
		}
	};

	/**
	 * BackboneViewMixin is a mixin created by react.backbone plugins,
	 * We are enhancing it with new (t)ranslate API
	 */
	React.BackboneViewMixin.t = function(textId) {
		// if the component has an i18n strings defined,
		// it will be used as key for the translation entries group
		if (typeof this.i18n === 'string') {
			textId = this.i18n + '.' + textId;
		}
		var str = I18n.t(textId);

		// React component doesn't support literal HTML entity, (read: http://facebook.github.io/react/docs/jsx-gotchas.html)
		// Therefore, we'll convert them into unicode character for convenience here

		//&nbsp;
		str = str.replace(/&nbsp;/gi, String.fromCharCode(160));

		return str;
	};

	/**
	 * Set global locale for Moment
	 * Use localized format supported by moment to generate the date:
	 *
	 * LT      8:30 PM (Time)
	 * L       09/04/1986 (Month numeral, day of month, year)
	 * l       9/4/1986
	 * LL      September 4 1986 (Month name, day of month, year)
	 * ll      Sep 4 1986
	 * LLL     September 4 1986 8:30 PM (Month name, day of month, year, time)
	 * lll     Sep 4 1986 8:30 PM
	 * LLLL    Thursday, September 4 1986 8:30 PM (Month name, day of month, day of week, year, time)
	 * llll    Thu, Sep 4 1986 8:30 PM
	 *
	 * @example
	 * ```
	 * moment(model.createdDate()).format('LL');
	 * ```
	 */
	moment.locale(translations._lang);

	//exports
	return I18n;
});
