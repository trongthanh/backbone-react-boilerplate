/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var Backbone = require('backbone');
	var AppRouter = require('app/AppRouter');
	var React = require('react');
	var AppModel = require('app/AppModel');
	var WelcomePage = require('jsx!app/views/pages/WelcomePage');
	var DashboardPage = require('app/views/pages/DashboardPage');

	/**
	 * Application View
	 *
	 * @class AppView
	 * @namespace app
	 */
	var AppView = Backbone.View.extend({
		el: '#app',

		router: AppRouter, //singleton
		dashboard: null,

		start: function() {
			console.log('App.start()');

			this.router.start(this);
			return this;
		},

		/**
		 * Render the target page according to URL hash,
		 * To be called by AppRouter
		 * @param  {String} page page ID
		 * @return {HTMLElement}      [description]
		 */
		render: function(page, options) {
			console.log('AppView::render ', page);
			var pageContainer = this.$('#main')[0];

			React.unmountComponentAtNode(pageContainer);

			switch (page) {
				case 'dashboard':
					if (!this.dashboard) {
						this.dashboard = new DashboardPage(AppModel, pageContainer);
					}
					this.dashboard.render(options);
					break;
				case 'welcome':
				/* falls through */
				default:
					React.renderComponent(
						WelcomePage({
							model: AppModel.session()
						}),
						pageContainer
					);
			}

			// Translate any text pre-existed in the markups which did not go through the React component stacks
			// Use the `data-i18n` attribute to assign the text id to be translated
			// This task is performance-consuming, avoid as much as possible
			// FIXME: currently there's no pre-existed translate items in index.html
			// this.$('[data-i18n]').each(function () {
			// 	this.innerHTML = I18n.t(this.getAttribute('data-i18n'));
			// });

			return this.el;
		}
	});

	// exports, Singleton
	return new AppView();
});