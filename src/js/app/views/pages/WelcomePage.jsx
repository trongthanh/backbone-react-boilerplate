/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');
	var LoginForm = require('jsx!app/views/components/LoginForm');

	/**
	 *
	 * @class WelcomePage
	 * @namespace
	 */
	var WelcomePage = React.createBackboneClass({
		i18n: 'welcome_page',

		render: function() {

			return (
				<div className="welcome-page">
					<div className="col-md-12 welcome-main">
						<div className="col-md-8 col-sm-7">
							<h2 className="welcome-title col-md-8">{this.t('welcome_title')}</h2>
						</div>
						<LoginForm model={this.props.model}/>
					</div>
					<footer className="welcome-footer">
						<div className="footer-left pull-left">
							<img src="img/logo.png" height="50" />&nbsp;
							<span className="copywrite">Copyright © 2014 Awesome Bank. {this.t('all_rights_reserved')}</span>
						</div>

						<div className="footer-right pull-right">
							<span>{this.t('conditions')}</span>
							&nbsp;|&nbsp;
							<span>{this.t('privacy')}</span>
						</div>
					</footer>
				</div>
			);
		}
	});

	// exports
	return WelcomePage;
});
