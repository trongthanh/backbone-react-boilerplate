/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');
	var Backbone = require('backbone');
	var ClientStreamComp = require('jsx!app/views/components/ClientStream');
	var ClientStreams = require('app/collections/ClientStreams');

	/**
	 *
	 * @class NewsPage
	 * @namespace
	 */
	var NewsPage = React.createBackboneClass({
		pageId: 'news',
		i18n: 'news_page',
		clientStream: null,

		getInitialState: function () {
			return {
				news: new Backbone.Collection(),
				clientStreams: new Backbone.Collection(),
				drafts: new Backbone.Collection(),
				weather: null
			};
		},

		componentDidMount: function () {
			this.clientStreams = new ClientStreams();
			this.clientStreams.on('sync', this.onSyncClientStream);
			this.clientStreams.fetch();
		},

		onSyncClientStream: function () {
			this.setState({clientStreams: this.clientStreams});
		},

		render: function() {
			return (
				<section className="news-page content">
					<h3>News Page</h3>
					<div className="row">
						<ClientStreamComp collection={this.state.clientStreams} />
					</div>
				</section>
			);
		}
	});

	// exports
	return NewsPage;
});
