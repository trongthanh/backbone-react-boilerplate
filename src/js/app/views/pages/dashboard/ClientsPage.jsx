/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');
	var SearchClientsList = require('jsx!app/views/components/SearchClientsList');
	// NOTE: Please try to decouple Model imports from Component as much as possible
	// It is best that the model/collection is passed from parents as props
	var AppModel = require('app/AppModel');

	/**
	 *
	 * @class ClientsPage
	 * @namespace
	 */
	var ClientsPage = React.createBackboneClass({
		i18n: 'clients_page',
		pageId: 'clients',

		getDefaultProps: function() {
			return {
				subpage: '',
				hash: '', //page's hash id, used to navigate
				deepTarget: '' //page's deeper URL hash target, should be handle within the page itself
			};
		},

		getInitialState: function() {
			return {
				filterText: ''
			};
		},


		handleFilterInput: function(event) {
			this.setState({filterText: event.currentTarget.value});
		},

		render: function() {
			console.log('this.props', this.props);
			var subpage = this.props.subpage;
			var hash = this.props.hash + '/' + subpage;

			var clientDetails;

			if (subpage) {
				clientDetails = (<span>Display page: {hash}</span>);
			} else {
				clientDetails = (<span>Please select a client</span>);
			}

			return (
				<section className="clients-page page row">
					<div id="submenu" className="left-panel clients-left-panel col-md-3">
						<header className="clients-search">
							<div className="search-box input -prepend inside search-form no-boarder">
								{/* BEGIN SEARCH BOX */}
								<span className="add-on"><i className="icon-search"></i></span>
								<input name="" type="text" className="no-boarder" placeholder={this.t('type_a_name')} value={this.state.filterText} onChange={this.handleFilterInput} />
								{/* END SEARCH BOX */}
							</div>
						</header>
						<div className="full-height scrollable">
							<SearchClientsList collection={AppModel.searchClients()} filterText={this.state.filterText} hash={this.props.hash} />
						</div>
					</div>
					<div className="content col-md-9">
						{clientDetails}
					</div>
				</section>
			);


		}
	});

	// exports
	return ClientsPage;
});
