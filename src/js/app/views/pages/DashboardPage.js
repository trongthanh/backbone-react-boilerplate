/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var AppRouter = require('app/AppRouter');
	var View = require('backbone').View;
	var React = require('react');
	var ClientsPage = require('jsx!app/views/pages/dashboard/ClientsPage');
	var NewsPage = require('jsx!app/views/pages/dashboard/NewsPage');
	var ContentHeader = require('jsx!app/views/components/ContentHeader');
	var I18n = require('app/helpers/I18n');


	/**
	 * DashboardPage wrapper.
	 *
	 * Example of a backbone view used in mix with ReactJS components
	 *
	 * @class DashboardPage
	 */
	var DashboardPage = View.extend({
		el: 'none', // avoid the view creating unwanted markups
		hash: 'dashboard',

		appModel: null,

		pageContainer: null,

		/**
		 * cache of current subpage
		 * @type {[type]}
		 */
		currentPage: null,

		events: {
			//empty
		},

		/**
		 * We'll inject AppModel passed AppView into this class to reduce dependencies
		 * @param  {[type]} appModel [description]
		 * @return {[type]}          [description]
		 */
		initialize: function(appModel, pageContainer) {
			this.appModel = appModel;
			this.pageContainer = pageContainer;
		},

		/**
		 * Render for the first time
		 * @return {[type]} [description]
		 */
		firstRender: function(/*options*/) {
			// do first time rendering here

		},

		render: function(options) {
			if (!this.isRendered) {
				this.isRendered = true;
				this.firstRender(options);
			}

			var ReactPage;
			var page = options.level1;
			var pageTitle = '';

			console.log('DashboardPage::render:level1:', page, '- level2:', options.level2, '- deepTarget:', options.deepTarget);

			switch (page) {
				case 'clients':
					ReactPage = ClientsPage;
					pageTitle = I18n.t('my_clients');
					break;
				case 'news':
					/* falls through */
				default:
					// this page don't have title, use logo instead
					page = 'news';
					ReactPage = NewsPage;
			}

			// clean up previous page
			React.unmountComponentAtNode(this.pageContainer);

			// render the page
			// This is an example of vanilla JS React Component setup
			this.el = React.renderComponent(
				React.DOM.div({},
					ContentHeader({pageTitle: pageTitle, onMenuClick: this.onMenuClick}),
					ReactPage({pageTitle: pageTitle, subpage: options.level2, deepTarget: options.deepTarget, hash: this.hash + '/' + page})
				),
				this.pageContainer
			).getDOMNode();

			// update active status
			// this.$('.menu-item').removeClass('active').filter('[data-ref="' + page + '"]').addClass('active');
		},

		onMenuClick: function() {
			alert('On menu click. For demo, redirecting to #logout which consequently redirect to #welcome');
			AppRouter.navigate('logout', {trigger: true});
		}


	});

	// exports
	return DashboardPage;
});