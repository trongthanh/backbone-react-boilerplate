/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');
	var classSet = React.addons.classSet;

	/**
	 *
	 * @class SearchClientsItem
	 * @namespace
	 */
	var SearchClientsItem = React.createBackboneClass({
		i18n: 'clients_page',

		getDefaultProps: function() {
			return {
			 	model: null,
				hash: '' //page's hash id, used to navigate
			};
		},

		getInitialState: function() {
			return {
				expanded: false
			};
		},

		handleToggleExpandClient: function(/*event*/) {
			this.setState({
				expanded: !this.state.expanded
			});
		},

		handleItemClick: function(event) {
			this.props.onItemClick(event);
		},

		render: function() {
			var client = this.props.model;
			var subsidiaries = client.subsidiaries();

			if (subsidiaries && subsidiaries.length) {
				// has subsidiaries
				var btnClass = classSet({
					'icon-minus': this.state.expanded,
					'icon-plus': !this.state.expanded
				});
				var liStyle = (this.state.expanded) ?
								{height: 60 + (60 * subsidiaries.length) + 'px'} : {height: '60px'};

				var subsidiaryList = subsidiaries.map(function(sub) {
					return <li key={sub.id} className="client"><a className="client-label" href={'#' + this.props.hash + '/' + sub.id} onClick={this.handleItemClick}><span>{sub.name()}</span></a></li>;
				}, this);

				return (
					<li className="client expandable" style={liStyle}>
						<button className="btn btn-success btn-cons-sm" onClick={this.handleToggleExpandClient} ><i className={btnClass}></i></button>
						{/* button + a; order is important here */}
						<a className="client-label" href={'#' + this.props.hash + '/' + client.id} onClick={this.handleItemClick}><span>{client.name()}</span></a>
						<ul className="client-list subclients-list">
							{subsidiaryList}
						</ul>
					</li>
				);
			} else {
				// no sub
				return <li className="client"><a className="client-label" href={'#' + this.props.hash + '/' + client.id} onClick={this.handleItemClick}><span>{client.name()}</span></a></li>;
			}
		}
	});

	// exports
	return SearchClientsItem;
});

