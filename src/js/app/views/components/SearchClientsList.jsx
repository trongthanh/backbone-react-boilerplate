/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');
	var SearchClientsItem = require('jsx!app/views/components/SearchClientsItem');

	/**
	 *
	 * @class SearchClientsList
	 * @namespace
	 */
	var SearchClientsList = React.createBackboneClass({
		i18n: 'clients_page',

		clients: null,

		getDefaultProps: function() {
			return {
			 	collection: null, // expect SearchClients collection
			 	filterText: '', // the text used to filter clients list
				hash: '', // page's hash id, used to navigate
				onItemClick: null // event callback to execute when one of this items is clicked
			};
		},

		handleFilterInput: function(event) {
			this.setState({filterString: event.currentTarget.value});
		},

		handleItemClick: function(event) {
			if (typeof this.props.onItemClick === 'function') {
				this.props.onItemClick(event);
			}
		},

		render: function() {
			var clientList;

			if (this.props.collection) {
				clientList = this.props.collection.searchClientsByName(this.props.filterText);
				clientList = clientList.map(function(client) {
					return <SearchClientsItem key={client.id} model={client} hash={this.props.hash} onItemClick={this.handleItemClick} />;
				}, this);
			}

			return (
				<ul className="clients-search-results clients-list">
					{clientList}
				</ul>
			);
		}
	});

	// exports
	return SearchClientsList;
});
