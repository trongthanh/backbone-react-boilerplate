/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Quy Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');
	var $ = require('jquery');
	var Backbone = require('backbone');
	var config = require('config');/* jshint ignore:line */
	var _ = require('lodash');/* jshint ignore:line */
	var Moment = require('moment');/* jshint ignore:line */

	var ClientStream = React.createBackboneClass({
		i18n: 'news_page',

		getInitialState: function () {
			return {

			};
		},

		getDefaultProps: function () {
			return {
				collection: new Backbone.Collection()
			};
		},

		componentDidMount: function () {
		},

		initComponents: function () {
		},

		$el: function () {
			//this.el() is method implemented in react.backbone.js
			return this.el() ? $(this.el()) : $('');
		},

		componentWillUnmount: function () {
			// this.$el().off();
		},

		/**
		 * componentDidUpdate occur when this component re-render or after the first time initialisation, but not being re-initialised
		 */
		componentDidUpdate: function () {
			// Only reinitialise related UI component such as jQueryUI, custom Select component
			this.initComponents();
		},

		render: function () {
			/* jshint ignore:start */
			var collection = this.collection();
			var models = collection.models ? collection.models.slice(0) : [];
			var self = this;
			var streamGroups;
			var groupContent = [];
			var streamListContent = [];
			if (models && models.length) {
				streamGroups = _.groupBy(models, function (model) {
					return model.get('date');
				});
				_.each(streamGroups, function (group, key) {
					var streams = group;
					var date = new Moment(key, config.DATE_FORMAT).format('MMM DD');
					streamListContent = [];
					streams.map(function (stream) {
						streamListContent.push(
							<li className="stream-group-item">
								<div className="item-content">
									<h3>{stream.get('owner')}</h3>
									<div>{stream.get('action')}</div>
								</div>
							</li>
						);
					});
					groupContent.push(
						<li>
							<h3 className="stream-group-title">{date}</h3>
							<ul className="stream-group">
								{streamListContent}
							</ul>
						</li>
					);
				})
				return (
					<div className="col-md-4" >
						<div className="tiles">
							<div className="tiles-body no-padding-bottom client-stream">
								<h2 className="b-b b-grey b-brown client-stream-header">{this.t('client_stream')}</h2>
								<ul className="client-stream-list tiles-body-content">
									{groupContent}
								</ul>
								<div className="tiles-footer clearfix">
									<a href="#" className="view-more">{this.t('view_more')}</a>
								</div>
							</div>
						</div>
					</div>
				);
			} else {
				return <div></div>;
			}

			/* jshint ignore:end */
		}
	});

	return ClientStream;
});
