/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');


	/**
	 * Content Header
	 *
	 * Header above content
	 *
	 * @class ContentHeader
	 * @namespace
	 */
	var ContentHeader = React.createBackboneClass({

		getDefaultProps: function() {
			return {
				pageTitle: ''
			};
		},

		/**
		 * Pass callback onMenuClick to the component to receive this event
		 * @param  {[type]} event [description]
		 * @return {[type]}       [description]
		 */
		handleMenuClick: function(event) {
			if (typeof this.props.onMenuClick === 'function') {
				this.props.onMenuClick(event);
			}
		},

		handleAlertClick: function() {
			alert('Demo component button click!');
		},

		render: function() {
			var pageTitleEl;
			var pageTitle = this.props.pageTitle;

			if (pageTitle) {
				pageTitleEl = (
					<li className="header-title"><h2>{pageTitle}</h2></li>
				);
			} else {
				pageTitleEl = (
					<li className="header-title header-logo">
						<img src="img/logo.png" height="30" />
					</li>
				);
			}

			return (
				/* BEGIN CONTENT HEADER */
				<div className="header-quick-nav clearfix">
					{/* BEGIN HEADER LEFT SIDE SECTION */}
					<div className="pull-left col-md-6">
						{/* BEGIN SLIM NAVIGATION TOGGLE */}
						<ul className="nav quick-section">
							<li className="quicklinks">
								<a className="inline" onClick={this.handleMenuClick} id="layout-condensed-toggle" href="javascript:;">
									<i className="icon-menu"></i>
								</a>
							</li>
							{pageTitleEl}
						</ul>
						{/* END SLIM NAVIGATION TOGGLE */}
					</div>
					{/* END HEADER LEFT SIDE SECTION */}
					{/* BEGIN HEADER RIGHT SIDE SECTION */}
					<div className="pull-right">
						{/* BEGIN HEADER NAV BUTTONS */}
						<ul className="nav quick-section">
							<li className="quicklinks">
								<a href="#dashboard/clients" id="global-find-clients" data-placement="bottom" data-trigger="click" ref="globalFindClientsBtn">
									<div className="iconset webfont">
										<i className="icon-link"></i>
										&nbsp; {this.t('my_clients')}
									</div>
								</a>
							</li>

							<li className="quicklinks">
								{/* BEGIN NOTIFICATION CENTER */}
								<a href="javascript:;" id="global-alerts" data-placement="bottom" data-content="" data-original-title="Notifications" onClick={this.handleAlertClick}>
									<div className="iconset webfont">
										<i className="icon-notification"><span className="badge badge-important animated bounceIn">3</span></i>
										&nbsp; {this.t('alert')}
									</div>
								</a>
							</li>
						</ul>
						{/* END HEADER NAV BUTTONS */}
					</div>
					{/* END HEADER RIGHT SIDE SECTION */}
				</div>
				/* END CONTENT HEADER */
			);
		}

	});

	// exports
	return ContentHeader;
});