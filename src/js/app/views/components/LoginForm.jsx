/** @jsx React.DOM */
/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var React = require('react');

	/**
	 *
	 * Expects app.models.Session as model
	 *
	 * @class LoginForm
	 * @namespace
	 */
	var LoginForm = React.createBackboneClass({
		i18n: 'login_form',
		// Just an example, we should inject model from controller or Appview
		// getDefaultProps: function() {
		// 	return {
		// 		model: require('app/AppModel').session()
		// 	};
		// },

		handleLoginIdChange: function(event) {
			this.model().set('loginId', event.target.value);
		},

		handlePasswordChange: function(event) {
			this.model().set('password', event.target.value);
		},

		handleFormSubmit: function(event) {
			event.preventDefault();
			this.model().login();
		},

		render: function() {
			return (
			/* jshint ignore:start */
				<form onSubmit={this.handleFormSubmit} className="col-md-4 col-sm-5 loginform">
					<h4 className=" col-md-12 form-item">{this.t('login_title')}</h4>
					<div className="col-md-12 form-item">
						<input type="text" id="welcome-login-id" className="input-edit col-md-12 col-sm-12" required
							value={this.model().loginId()}
							onChange={this.handleLoginIdChange}></input>
						<label htmlFor="welcome-login-id" className="label-edit" >{this.t('id')}</label>
					</div>
					<div className="col-md-12 form-item">

						<input type="password" id="welcome-login-password" className="input-edit col-md-12 col-sm-12" required
							value={this.model().password()}
							onChange={this.handlePasswordChange}></input>
						<label htmlFor="welcome-login-password" className="label-edit" >{this.t('password')}</label>
					</div>
					<div className="col-md-12 form-item">
						<button className="btn btn-primary col-md-12 col-sm-12">{this.t('sign_in')}</button>
					</div>
					<a href="#" className="forget-pass" data-toggle="modal" data-target=".password-request-modal">{this.t('cant_access')}</a>
				</form>
			/* jshint ignore:end */
			);
		}
	});

	// exports
	return LoginForm;
});