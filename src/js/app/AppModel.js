/* © 2014 Aleph-labs
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseModel = require('app/models/BaseModel');
	var Session = require('app/models/Session');
	var User = require('app/models/User');
	var LocalStorage = require('app/helpers/LocalStorage');
	var SearchClients = require('app/collections/SearchClients');

	/**
	 * The Singleton Application Model which
	 * facilitate all global models for this app
	 *
	 * @class AppModel
	 * @namespace app
	 */
	var AppModel = BaseModel.extend({

		defaults: {
			session: new Session(),
			user: new User(),
			searchClients: new SearchClients()
		},

		session: function() { return this.get('session'); },
		user: function() { return this.get('user'); },
		searchClients: function() { return this.get('searchClients'); },

		initialize: function() {
			var session = this.session();
			var user = this.user();
			var storedSession = LocalStorage.getItem('session');

			session.on('change:sessionId', function() {
				if (session.sessionId()) {
					// save this session to LocalStorage
					LocalStorage.setItem('session', session.toJSON());
					// console.debug('Store session:', LocalStorage.getItem('session', 'string'));
					// name will be return from session
					user.set('name', session.name());
					// load brief clients list belong to this RM
					this.searchClients().fetch();
				} else {
					// no session, just clear the stored one
					LocalStorage.removeItem('session');
				}

			}, this);

			//check stored login session here
			if (storedSession && storedSession.expire.getTime() > Date.now()) {
				session.set(storedSession);
				console.debug('AppModel::initilize:storedSession.expire', storedSession.expire);
			}
		}
	});

	// exports
	return new AppModel();
});