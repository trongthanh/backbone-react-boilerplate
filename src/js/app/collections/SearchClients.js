/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseCollection = require('app/collections/BaseCollection');
	var config = require('config');
	var SearchClient = require('app/models/SearchClient');

	/**
	 * @class SearchClients
	 */
	var SearchClients = BaseCollection.extend({
		model: SearchClient,

		initialize: function() {
			this.on('sync', this._generateSubsidiaries, this);
		},

		url: function() {
			return this.urlRoot + config.SERVICE_BRIEF_CLIENTS_LIST;
		},

		parse: function (response) {
			return response.clients;
		},

		/**
		 * Create subsidiaries array in the model act as group
		 */
		_generateSubsidiaries: function() {
			console.log('SearchClients::_generateSubsidiaries,on-sync');
			var byGroupList = this.groupBy('groupId');

			this.each(function(model) {
				var childCompanies = byGroupList[model.id];
				if (childCompanies) {
					model.set({subsidiaries: childCompanies});
				}
			});
			// console.log('collection', this);
		},

		/**
		 * Filter and do subsidiraries filtering
		 * @param  {[type]} q [description]
		 * @return {Array}   Array of raw objects of SearchClient model (similar toJSON())
		 * FIXME: require unit test
		 */
		searchClientsByName: function(q) {
			var searchResults, topIndexList, finalList;

			q = q.toLowerCase(); // ignore case

			if (!q) {
				// empty strings or null, return the whole collection with
				console.log('SearchClients::searchClientsByName:q', q, ':return all');
				// get the list of all top level companies
				finalList = this.filter(function(model) {
					return (model.groupId() === null);
				});

				return finalList;
			}
			console.log('SearchClients::searchClientsByName:q', q);

			topIndexList = []; // this array is for quick search of top level client using indexOf()
			finalList = [];

			searchResults = this.filter(function(model) {
				return (model.name().toLowerCase().indexOf(q) !== -1);
			});

			// regenerate subsidiaries and nested groups
			searchResults.forEach(function(model) {
				var owner, ownerIdx;
				//look through clients which are subsidiaries
				if (model.groupId()) {
					// get the owner to add itself to the subsidiaries array
					owner = this.get(model.groupId()).clone();
					ownerIdx = topIndexList.indexOf(owner.id);

					if (ownerIdx >= 0) {
						// if this owner has been added before
						if (!finalList[ownerIdx].matchedOwner) {
							finalList[ownerIdx].subsidiaries().push(model);
						}
					} else {
						// owner is not added before
						owner.set('subsidiaries', [model]);
						topIndexList.push(owner.id);
						finalList.push(owner);
					}
				} else {
					// for matched owners, include all of its sub
					// TODO: BIG NOTE, I'm assuming the owners list come before subsidiaries list
					owner = model.clone();
					owner.matchedOwner = true; // this flag is to prevent the subsidiaries to be readded to its list
					topIndexList.push(owner.id);
					finalList.push(owner);
				}
			}, this);

			return finalList;
		}

	});

	return SearchClients;
});
