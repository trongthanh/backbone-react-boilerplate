/* © 2014 Aleph-labs.com
 * @author Quy Tran
 */
define(function(require) {
	'use strict';
	// imports
	var Backbone = require('backbone');
	var config = require('config');

	var BaseCollection = Backbone.Collection.extend({
		urlRoot: config.SERVICE_URL_ROOT
	});

	return BaseCollection;
});
