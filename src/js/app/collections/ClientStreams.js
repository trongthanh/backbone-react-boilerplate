/* © 2014 Aleph-labs.com
 * @author Quy Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseCollection = require('app/collections/BaseCollection');
	var config = require('config');
	var ClientStream = require('app/models/ClientStream');

	var ClientStreams = BaseCollection.extend({
		model: ClientStream,

		url: function() {
			return this.urlRoot + config.SERVICE_CLIENT_STREAM;
		},

		parse: function (response) {
			return response.clientStreams;
		}
	});

	return ClientStreams;
});
