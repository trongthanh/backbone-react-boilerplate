/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseModel = require('app/models/BaseModel');

	/**
	 * User info model
	 *
	 * @class User
	 * @namespace app.models
	 */
	var User = BaseModel.extend({

		defaults: {
			name: ''
		},

		name: function() { return this.get('name'); }

	});

	// exports
	return User;
});