/* © 2014 Aleph-labs.com
 * @author Quy Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseModel = require('app/models/BaseModel');
	var config = require('config');

	var Client = BaseModel.extend({
		url: function() {
			return this.urlRoot + config.SERVICE_CLIENT_LIST;
		}
	});

	return Client;
});
