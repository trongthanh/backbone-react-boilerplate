/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	// NOTE: we load the backbone services here to make sure all models will sync through our AJAX wrapper
	var Backbone = require('app/helpers/BackboneServices');
	var config = require('config');

	/**
	 * Base model for this app
	 *
	 * @class BaseModel
	 * @namespace app.models
	 */
	var BaseModel = Backbone.Model.extend({
		urlRoot: config.SERVICE_URL_ROOT,

		/**
		 * Getter / setter function helper.
		 *
		 * Depending on number of arguments of the method, make the method as getter or setter
		 * @example
		 * // Define getset method like so
		 * name: function() { this._getset('name', arguments); }
		 * @return {*} if getter return the equivalent attribute / setter return this model
		 */
		_getset: function(attr, args) {

			if (args.length === 0) {
				// if no arguments, assume getter
				return this.get(attr);
			} else {
				// if getset method has arguments, assume setter
				if (args.length > 1) {
					console.warn('Model setter', attr, 'has more than one arguments:', args );
				}
				return this.set(attr, args[0]);
			}
		}
	});

	// exports
	return BaseModel;
});