/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseModel = require('app/models/BaseModel');
	var config = require('config');

	/**
	 * Session model, manage login and session states
	 *
	 * @class Session
	 * @namespace app.models
	 */
	var Session = BaseModel.extend({
		url: function() {
			return this.urlRoot + config.SERVICE_LOGIN;
		},

		defaults: {
			loginId: '',
			password: '',
			expireIn: NaN, // number of millisecond
			expire: '' // Date time when it expires
			//extra (after save)
			//sessionId: ''
			//name: '',
		},

		initialize: function() {

		},

		//getter/setter
		loginId   : function() { return this._getset('loginId', arguments); },
		password  : function() { return this._getset('password', arguments); },
		sessionId : function() { return this._getset('sessionId', arguments); },

		//getter only
		name      : function() { return this.get('name'); },
		expireIn  : function() { return this.get('expireIn'); },
		expire : function() {
			return this.get('expire') || this.extendExpiration();
		},

		/**
		 * Override this method to return the expire value properly
		 * @override
		 * @return {[type]} [description]
		 */
		toJSON: function() {
			var obj = BaseModel.prototype.toJSON.apply(this, arguments);

			if (obj.expireIn && !obj.expire) {
				obj.expire = this.expire(); // calculate expire via this getter
			}
			return obj;
		},

		/**
		 * Log user in using built-in sync method
		 * @return {[type]} [description]
		 */
		login: function() {
			this.save();
		},

		/**
		 * Log out of session and clear local storage
		 * @return {[type]} [description]
		 */
		logout: function() {
			this.set({
				loginId: '',
				password: '',
				expireIn: NaN, // number of millisecond
				expire: '', // Date time when it expires
				sessionId: '',
				name: ''
			});
		},

		/**
		 * Check whether user is logged in
		 * @return {Boolean} [description]
		 */
		isLoggedIn: function() {
			return !!this.get('sessionId');
		},

		extendExpiration: function() {
			// access raw attributes to avoid going through the override set
			this.set('expire', new Date(Date.now() + this.expireIn()));
			return this.get('expire');
		}

	});

	// exports
	return Session;
});