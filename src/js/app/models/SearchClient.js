/* © 2014 Aleph-labs.com
 * @author Thanh Tran
 */
define(function(require) {
	'use strict';
	// imports
	var BaseModel = require('app/models/BaseModel');

	var SearchClient = BaseModel.extend({

		defaults: {
			name: '', // name of the company
			groupId: null, // the group id which this company belong to
			subsidiaries: null // array of subsidiaries, this will be result of manipulation in the collection
		},

		// getters
		name:         function() { return (this.get('name') || ''); },
		groupId:      function() { return this.get('groupId'); },
		subsidiaries: function() { return this.get('subsidiaries'); }


	});

	return SearchClient;
});
