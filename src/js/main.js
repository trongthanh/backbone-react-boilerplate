/* © 2014 aleph-labs.com
 * @author Thanh Tran
 */
'use strict';
//Append ?lang=en in browser address to open the app with English language, default is es
var lang = (window.location.href.toLowerCase().indexOf('lang=es') >= 0) ? 'es' : 'en';
require.config({
	baseUrl: 'js/lib', //make modules in lib having no leading namespace

	paths: { //making shortcuts for these popular modules / namespace
		'app': '../app',
		'config': '../config',
		'polyfills': '../polyfills',
		'data': '../../data', //folder to contains dynamically loaded json
		'data/translations': '../../data/translations-' + lang + '.json' //translations English by default
		//plugins map

	},
	map: {
		'backbone': {
			'underscore': 'lodash' //remap 'underscore' dependency of backbone to 'lodash'
		},
		'react.backbone': {
			'underscore': 'lodash'
		}
	},
	shim: { //list all dependency of plugins which doesn't support AMD
		'bootstrap': { deps: ['jquery'] }
	},
	jsx: {
		fileExtension: '.jsx'
	}
});

/**
 * Modernizr is loaded synchronously in the header
 * Define a named module here to prevent it to be redownload when other modules require it
 */
/*global Modernizr*/
define('modernizr', [], Modernizr);

/**
 * First load all global modules which are required before AppView is loaded.
 * Global modules can be:
 * - Prototype augmenting helpers
 * - jQuery plugins
 */
require(
[
	'polyfills', // polyfills for this project
	'jquery', // duh
	'lodash', // utilities
	'modernizr', // help detect HTML5 features
	'react.backbone', // help binding Backbone models/collections with React component
	'app/helpers/I18n' // Localization wrappers
],
function(global, $, _) {
	//load AppView async and start it
	require(['app/AppView'], function(AppView) {
		// document ready
		$(function() {
			// expose the app to window
			window.app = AppView.start();
		});
	});

	/*@exclude*/
	/**
	 * Thanh: For the sake of code clarity and quality, I'm destroying these global variables to avoid missuses
	 */
	// Destroy global jQuery `$`
	window.$ = function() {
		throw new Error('Did you forget to import `jquery`? Please do not use global jQuery `$` for clarity sake.');
	};
	// Destroy global lodash `_`
	var noLodash = {};
	var noMethod = function() {
		throw new Error('Did you forget to import `lodash`? Please do not use global lodash `_` for clarity sake.');
	};

	for (var method in _) {
		if (typeof _[method] === 'function') {
			noLodash[method] = noMethod;
		}
	}
	window._ = noLodash;
	/*@endexclude*/


	// first export a null object
	window.app = null;
});

