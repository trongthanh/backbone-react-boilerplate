/**
 * Most snippets here are copied from MDN document
 */
/*jshint strict:false,eqeqeq:false,bitwise:false,unused:false,curly:false,node:true*/
// UMD, see https://github.com/umdjs/umd/blob/master/returnExports.js
(function (root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define([], factory);
	} else if (typeof exports === 'object') {
		// Node. Does not work with strict CommonJS, but
		// only CommonJS-like environments that support module.exports,
		// like Node.
		module.exports = factory();
	} else {
		// Browser globals (root is window)
		root.returnExports = factory();
	}
}(this, function () {

	/**
	 * Object.assign polyfill
	 * Ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
	 *
	 * This ES6 API required for the convenient spread attributes in JSX, e.g. <Component {...this.props} />
	 */
	if (!Object.assign) {
		Object.defineProperty(Object, "assign", {
			enumerable: false,
			configurable: true,
			writable: true,
			value: function(target, firstSource) {
				"use strict";
				if (target === undefined || target === null)
					throw new TypeError("Cannot convert first argument to object");

				var to = Object(target);

				var hasPendingException = false;
				var pendingException;

				for (var i = 1; i < arguments.length; i++) {
					var nextSource = arguments[i];
					if (nextSource === undefined || nextSource === null)
						continue;

					var keysArray = Object.keys(Object(nextSource));
					for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
						var nextKey = keysArray[nextIndex];
						try {
							var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable)
								to[nextKey] = nextSource[nextKey];
						} catch (e) {
							if (!hasPendingException) {
								hasPendingException = true;
								pendingException = e;
							}
						}
					}

					if (hasPendingException)
						throw pendingException;
				}
				return to;
			}
		});
	}

	return this;
}));